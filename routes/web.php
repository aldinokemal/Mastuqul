<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'home\HomeGetController@index')->name('home.home');
Route::get('/submit', 'home\HomeGetController@submit');

Route::group(['middleware' => 'login.access'], function(){
	Route::get('/login', 'LoginController@login')->name('login.login');
	Route::post('/auth/login', 'LoginController@auth');

	// Reset Password
	Route::post('/login/resetpassword', 'LoginController@reset');
	Route::get('/login/newpassword/{email}tql/{token}', 'LoginController@viewResetPassword');
	Route::post('/login/newpassword/{email}tql/{token}', 'LoginController@newPassword');
});


Route::get('/auth/logout', 'LoginController@logout')->middleware('admin.access');

// Route::group(['prefix' => 'admin'], function(){
Route::group(['prefix' => 'admin', 'middleware' => 'admin.access'], function(){
	Route::get('/', 'admin\AdminGetController@index')->name('admin.dashboard');
	Route::get('/get/chartdata', 'admin\AdminGetController@chartData');

	Route::get('/get/numberinfo', 'admin\AdminGetController@getNumberInfo');
	ROute::post('/post/updatenumber', 'admin\AdminPostController@updatePhoneNumber');

	Route::post('/post/updatepassword', 'admin\AdminPostController@updatePassword');
});
