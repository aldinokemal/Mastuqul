const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/login/js/login.js', 'public/js')
   .sass('resources/assets/login/sass/login.scss', 'public/css');

mix.js('resources/assets/admin/js/admin.js', 'public/js');
   // .sass('resources/assets/admin/sass/admin.scss', 'public/css');