/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

import Vue from 'vue'
import Toasted from 'vue-toasted'

// library imports
import VeeValidate from 'vee-validate'
import { Validator } from 'vee-validate';
import VueSweetAlert from 'vue-sweetalert'

// router setup
// import routes from './routes/routes';

// Plugin
import App from './components/login/login.vue'


// Plugin Setup
Vue.use(VeeValidate)
Vue.use(Toasted)
Vue.use(VueSweetAlert)


// Update Vee Validation Message
const dictionary = {
  	en: {
	  	custom: {
	  		email:{
		     	required: () => 'Email tidak dapat kosong',
		     	email: () => 'Email tidak valid'
		    },
		    sandi:{
		    	required: () => 'Kata sandi tidak dapat kosong'
		    }
	  	}
	    
  }
}
Validator.updateDictionary(dictionary);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App),
})

