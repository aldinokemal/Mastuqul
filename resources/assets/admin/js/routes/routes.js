import NotFound from '../components/Errors/NotFound.vue'

import Chart from '../components/Dashboard/View/Chart.vue'
import Setting from '../components/Dashboard/View/Setting.vue'

const routes = [
	{ 
		path: '/', 
		redirect: '/chart' 
	},
	// { 
	// 	path: '/chart', 
	// 	component: Chart 
	// },
	// { 
	// 	path: '/setting', 
	// 	component: Setting
	// },
	{ path: '*', component: NotFound }
]

export default routes