/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

// library imports
import Vue from 'vue'
import Vuetify from 'vuetify'

import Toasted from 'vue-toasted'
import VueRouter from 'vue-router'
import VueSweetAlert from 'vue-sweetalert'


// router setup
import routes from './routes/routes'
import App from './components/App.vue'

// Plugin Setup
Vue.use(Toasted)
Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.use(VueSweetAlert)


// configure router
const router = new VueRouter({
    routes,
})

Vue.component('vue-loader', require('vue-spinner/src/ClipLoader.vue'))


/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    render: h => h(App),
})

