<!DOCTYPE html>
<!--[MASTUQUL]>
    DEVELOPED BY MASTUQUL TIM
<![MASTUQUL]-->
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta content='width=device-width, initial-scale=0.8, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#00838F">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#00838F">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#00838F">

	<title>Admin Panel | MASTUQUL</title>
	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet" type="text/css">
	<link href="/css/vuetify.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="app"></div>
	
	<script src="/js/admin.js"></script>
</body>
</html>