<!DOCTYPE html>
<!--[MASTUQUL]>
    DEVELOPED BY MASTUQUL TIM
<![MASTUQUL]-->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta content='width=device-width, initial-scale=0.8, maximum-scale=1.0, user-scalable=0' name='viewport'/>    
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#7B9BA8">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#7B9BA8">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#7B9BA8">
	<title>Login | Mastuqul</title>
	<link rel="stylesheet" href="/css/login.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
</head>
<body>
	<div id="app"></div>

	<script type="text/javascript" src="/js/login.js"></script>
</body>
</html>