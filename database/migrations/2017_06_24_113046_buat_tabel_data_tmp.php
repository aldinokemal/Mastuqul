<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelDataTmp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datas_tmp', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amonia', 8, 2);
            $table->float('suhu', 8, 2);
            $table->float('kelembaban', 8, 2);
            $table->timestampTz("created_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datas_tmp');
    }
}
