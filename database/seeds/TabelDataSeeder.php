<?php

use Illuminate\Database\Seeder;

class TabelDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=11; $i < 31 ; $i++) { 
    		DB::table('datas')->insert([
	            'amonia' 	 => rand(17,23)/10,
	            'suhu' 		 => rand(27,30),
	            'kelembaban' => rand(70,90),
                'created_at' => "2017-07-10 20:$i:28",
	        ]);	
    	}
    }
}
