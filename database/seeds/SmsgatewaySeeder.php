<?php

use Illuminate\Database\Seeder;

class SmsgatewaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('smsgateway')->insert([
        	'penerima' => '089685024091'
        ]);
    }
}
