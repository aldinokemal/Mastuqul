<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $dataResetEmail;

    public function __construct($dataResetEmail)
    {
        $this->dataResetEmail = $dataResetEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('login.mail.resetpassword', ['datas' => $this->dataResetEmail])
                    ->subject('MASTUQUL | Reset Password');
    }
}
