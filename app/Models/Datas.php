<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Datas extends Model
{
	protected $table = 'datas';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];
}
