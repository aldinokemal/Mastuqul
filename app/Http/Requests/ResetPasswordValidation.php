<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newpassword' => 'required|string|min:4',
        ];
    }

    public function messages()
    {
        return [
            'newpassword.required' => 'Kata sandi tidak dapat kosong',
            'newpassword.string'   => 'Tipe data password salah',
            'newpassword.min'      => 'Kata sandi minimal 4 karakter',
        ];
    }
}
