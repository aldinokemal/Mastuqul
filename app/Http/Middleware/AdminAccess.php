<?php

namespace App\Http\Middleware;

use Closure;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(auth('admin')->check());
        // dd($request->session()->all());
        if (auth('admin')->check()) {
            return $next($request);
        }
        return redirect('/login/');
    }
}
