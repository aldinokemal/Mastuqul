<?php

namespace App\Http\Controllers\home;

use DB;
use App\Models\Datas;
use Illuminate\Http\Request;
use App\Events\UpdateChartPusher;
use App\Http\Controllers\Controller;

class HomeGetController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function submit(Request $request)
    {

        if (is_null($request['a']) || is_null($request['s']) || is_null($request['k'])) {
            return response()->json([
                'succes' => false
            ]);
        }

        $datas = Datas::take(15)->orderBy('id', 'desc')->get();
        Datas::insert([
            'amonia' => $request['a'],
            'suhu' => $request['s'],
            'kelembaban' => $request['k']
        ]);

        DB::table('datas_tmp')
            ->insert([
                'amonia' => $request['a'],
                'suhu' => $request['s'],
                'kelembaban' => $request['k']
            ]);

        event(new UpdateChartPusher('Update Chart NOW'));

        $total = DB::table('datas_tmp')->count();

        dump($total);

        if ($total >= 15) {
            $amoniaData = DB::table('datas_tmp')->take(15)->orderBy('id', 'desc')->get();
            foreach ($amoniaData as $data) {
                $list[] = $data->amonia;
            }
            $amoniaValue = (array_sum($list) / 15);
            dump($amoniaValue);
            if ($amoniaValue >= 2.0) {
                DB::table('datas_tmp')->truncate();
                // ============  Send SMS gateway  ============= //
                $SMS = new SmsGateway(config('smsgateway.email'), config('smsgateway.password'));
                $sms_message = "Segera bersihkan toilet, dalam 15 menit terakhir amonia mencapai ". $amoniaValue."ppm\r\n\r\nMASTUQUL";
                $numberTarget = DB::table('smsgateway')->first()->penerima;
                $sending = $SMS->sendMessageToNumber($numberTarget, $sms_message, config('smsgateway.device'));
                return response()->json([
                    'succes' => true
                ]);
            }

        }
        return 'berhasil';
    }
}
