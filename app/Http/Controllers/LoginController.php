<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use App\Events\ForgetPasswordCheck;
use App\Http\Requests\LoginAdminValidation;
use App\Http\Requests\ResetPasswordValidation;
use Illuminate\Contracts\Encryption\DecryptException;

class LoginController extends Controller
{
	// GET
    public function login(Request $request)
    {
    	return view('login.login');
    }

    // post
    public function auth(LoginAdminValidation $request)
    {    
    	// $a = Auth::attempt($request->all());
    	// return response()->json([
    	// 		'success' => $a
    	// 	]);
    	$auth = auth('admin');
    	if($auth->attempt($request->all())){
    		// dd($auth->check());
    		// dd(session()->all());
    		// return redirect()->intended('/admin');
    		return response()->json([
				'success'  => true,
				'redirect' => '/admin/'
    		]);
    	}
    	
    	return response()->json([
				'success'  => false,
				'notification' => 'Email atau sandi salah'
    		]);
    }

    public function reset(Request $request)
    {

        $data = User::where('email','=', $request['email'])->first();
        if(!is_null($data)){
            event(new ForgetPasswordCheck($data));
        }
        // $data = 'aww';
        return response()->json([
                'success'  => true,
                'Data' => $data
            ]);
    }

    // GET
    public function viewResetPassword(Request $request, $email, $token)
    {        
        try {
            $decryptEmail = decrypt($email);
            $decryptToken = decrypt($token);
        } catch (DecryptException $e) {
            // return redirect()->back()->withError('Try the system ? try harder !');
            abort(404);
        }
        User::where([
                'email'          => $decryptEmail,
                'remember_token' => $decryptToken
            ])->firstOrFail();

        return view('login.resetpassword');
    }

    // POST
    public function newPassword(ResetPasswordValidation $request, $email, $token)
    {
        try {
            $decryptEmail = decrypt($email);
            $decryptToken = decrypt($token);
        } catch (DecryptException $e) {
            // return redirect()->back()->withError('Try the system ? try harder !');
            abort(404);
        }

        User::where([
                'email'          => $decryptEmail,
                'remember_token' => $decryptToken
            ])
            ->update([
                'password' => bcrypt($request->newpassword),
                'remember_token' => str_random('50'),
            ]);
                
        echo "<script>alert('Kata sandi berhasil di perbaharui'); window.location.href='/login';</script>";
    }

    // GET Logout dari admin
    public function logout(Request $request)
    {
        auth('admin')->logout();
        return redirect()->back();
    }
}
