<?php

namespace App\Http\Controllers\admin;

use Hash, DB;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateReceiverNumber;

class AdminPostController extends Controller
{
    public function updatePhoneNumber(UpdateReceiverNumber $request)
    {
        $update = DB::table('smsgateway')->update($request->all());

        return response()->json([
            'success' => true
        ]);
    }

    public function updatePassword(Request $request)
    {
    	if(Hash::check($request['currentPassword'], auth('admin')->user()->password)){
    		User::find(auth('admin')->id())->update(['password' => bcrypt($request['newPassword'])]);
    		return response()->json([
	            'success' => true
	        ]);
    	}
    	else{
    		return response()->json([
	            'success' => false
	        ]);
    	}
    	
    }
}
