<?php

namespace App\Http\Controllers\admin;

use DB;
use App\Models\Datas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminGetController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.dashboard');
    }

    public function chartData(Request $request)
    {
        $datas = Datas::take(15)->orderBy('id', 'desc')->get();
        foreach ($datas as $data) {
            $amonia[] = $data->amonia;
            $suhu[] = $data->suhu;
            $waktu[] = substr($data->created_at->toDateTimeString(), 11, 5);
            $kelembaban[] = $data->kelembaban;
        }

        return response()->json([
            'amonia' => array_reverse($amonia),
            'suhu' => array_reverse($suhu),
            'waktu' => array_reverse($waktu),
            'kelembaban' => array_reverse($kelembaban)
        ]);
    }

    public function getNumberInfo(Request $request)
    {
        $data = DB::table('smsgateway')->first();
        return response()->json([
            'penerima' => $data->penerima,
        ]);
    }
}
