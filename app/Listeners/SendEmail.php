<?php

namespace App\Listeners;

use Mail;
use App\Mail\ResetPassword;
use App\Events\ForgetPasswordCheck;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForgetPasswordCheck  $event
     * @return void
     */
    public function handle(ForgetPasswordCheck $event)
    {
        Mail::to($event->dataResetEmail->email)
            ->send(new ResetPassword($event->dataResetEmail));
    }
}
