# MASTUQUL #

Mastuqul merupakan IOT dari adruino yang dikembangkan oleh mahasiswa Informatika UMS 2016


## Langkah Penggunaan
1. git clone project ini atau download
1. ekstract file (jika download via zip/tar)
1. jalankan ```composer install```
1. rename **.env.example** => **.env**
1. setting .env:
    * ```DB_DATABASE=[your database]```
    * ```DB_USERNAME=[your_username]```
    * ```DB_PASSWORD=[your_password]```
    * ```MAIL_PASSWORD=[hidden_password]```
    * ```SMS_PASSWORD=[hidden_password]```

1. masuk pada directory folder via terminal / CMD (**shift + right click** pada folder, kemudian open with **command prompt**)
1. jalankan ```php artisan migrate --seed```
1. pastikan install [NodeJS dan NPM](https://nodejs.org/en/download/) lalu lakukan ```npm install``` (karena project dikembangkan dengan syntax ES6 vue JS)
1. jalankan ```npm run watch-poll```
1. buka terminal / cmd baru dan masuk lokasi folder mastuqul kemudian jalankan ```php artisan serve```
1. buka **localhost:8000**

## Note
* ```npm run watch-poll```: untuk versi development (pengembangan)
* ```npm run production```: untuk versi produksi (live server)
* ```php artisan migrate --seed```: membuat database dan insert secara otomatis

### Tampilan Dashboard
!['Dashboard'](https://image.ibb.co/eKCt8G/Screenshot_from_2017_11_13_16_13_17.png "Dashboard")

### Tampilan Setting
!['Setting'](https://image.ibb.co/ie3daw/Screenshot_from_2017_11_13_16_14_19.png "Setting")
